# General Linear Model: get 1st level betas

## 1st level analysis: univariate beta weights for load conditions

We conducted a 1st level analysis using SPM12 to identify beta weights for each load condition separately. Design variables included stimulus presentation by load (4 volumes; parametrically modulated by sequence position), onset cue (no mod.), and probe (2 volumes, parametric modulation by RT). Design variables were convolved with a canonical HRF, including its temporal derivative as a nuisance term. Nuisance regressors included 24 motion parameters 112, as well as continuous DVARS estimates. Autoregressive modelling was implemented via FAST. Output beta images for each load condition were finally averaged across runs.

* a_glm: 1st level specification for batch

    * batches saved in `D_batch1stLevel_v2`
    * first level betas saved in `F_GLM1stLevelout_v2/ID/`